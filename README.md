# README for Team Dev Dojo II project#

## NOTE:  This stage II project assumes that you have completed the [first stage](https://bitbucket.org/urbanspectra/team-dev-dojo).

* Feel free to skim this project readme, but stage I precedes stage II.

* SECURE Solo, Local: Time to complete = 10 minutes

* Solo, Remote: Time to complete = 10 minutes

* Team, Remote: Time to complete = 15 minutes

* Team, [Advanced](http://www.gitready.com): Ongoing

### What is this repository used for? ###

* This is stage two of the three stage process to start working with app dev teams on github.
* We start by securing the solo repositories we created in [urbanspectra-dev-dojo stage 1](https://bitbucket.org/urbanspectra/team-dev-dojo).
---
---
### How do I get started with *Solo Remote* ? ###

1. Get free accounts at BOTH github.com and bitbucket.org.
* Both web sites support the tool named git.
* Create the same login id and password on both sites.
* NOTE: Bitbucket provides free private repositories. Github does not.

2. Create [your own github public personal project repo](https://help.github.com/articles/create-a-repo/).
* Name the project 'first-public-remote-project'.
* Make it public. You can add readme and license later.
* Look at the url of your first remote github repo.
* You will use that when you clone to local.

3. Clone that remote public personal github.com repo to your local system:  
```
CLONE TO LOCAL: $ cd ~/code-projects; git clone [enter github repo project url here]
ENTER CLONE: $ cd first-public-remote-project; 
LOOK AROUND: $ git remote -v; ls -al; git status
```
* Note that you can optionally rename local repo folder via $ git clone [remote url] [new-local-repo-folder-name]

4. Find a buddy to add as a contributor to your personal github project.
* Use their github id.
* Give them read and write access.
* Have them do the same from their end at github in their own personal pubic project.

5. Email jdonson@gmail with the requirements below to get write access to both private (bitbucket) and public (github) Team-Dev-Dojo projects.
* github login id
* bitbucket login id

6. Create [personal github web page](https://pages.github.com/) in your LOCAL personal github project
```
$ echo '' > README.md ??  REDO THIS SECTION
$ git status
$ git add all??
$ git commit -m 'First github personal web page.'
$ git status
$ git push
$ git status
```
7. View this personal page in your browser. ??

8. Use personal project [issues(https://guides.github.com/features/issues/) in your own project to manage to-do list.

9. Do all of this again, but this time at bitbucket, and instead create a private repo.

10.  Create your personal local .gitignore file, which also ignores itself:
```
$ mkdir hide
$ echo -e 'hide/\n.gitignore' >> .gitignore
$ git status
$ git add .
$ git status
$ git commit -m 'Added .gitignore, but it ignores itself so it does not get pushed to remote.'
$ git status                # Nothing to push, because new file and new folder are both ignored.
```
* NOTE:  Hide stuff that you do not want to push 
* OS X Users, .DS_Store files can appear spontaneously,
* so to leave them out of commits, run the following to append .gitignore file:
```
$ pwd
$ echo '.DS_Store' >> .gitignore
$ cat .gitignore
```

11. Mess up by tracking erroneous file, then undo by reverting to last commit:
```
$ echo 'alkejhkhj' > mistake.txt
$ ls -al
$ git status
$ git add .
$ git status
$ git reset HEAD
$ git status
```

12. Amend your last commit log message:
```
$ git commit --ammend
```

13. Follow instructions to create a [bitbucket project web page](https://pages.bitbucket.io/).

---

### How do I get started on *Team Remote* ? ###

1. Secure your access to github.com. ??
2. Secure your access to bitbucket.com ??

3. Clone the [github public team-dev-dojo](https://github.com/jeremy-donson/team-dev-dojo) first to prevent being delayed:

```
$ mkdir ~/code-projects/; cd ~/code/projects; git clone https://github.com/jeremy-donson/team-dev-dojo
```
* NOTE: Assuming that you are not risking loss of work in a local project,
* it is fine to discard local copy and reclone to local again.

4. Enter cloned repo, look around, and then create new directory with sample text file:
```
$ cd team-dev-dojo; ls -al; git status; git remote -v
$ mkdir useless-dir; echo 'useless text' > useless-dir/useless-file.txt
```

5. Check your email account to see if you have been given access to the bitbucket private version of this team dev dojo.

6. You now have two public projects cloned to your local drive:

* Personal Project: first-public-remote-project from github, where you created new personal public github page.

* Team Project: team-dev-dojo from github
* => Work solo against this TEAM repo by in a new folder named to reflect your github login id.

7. Start to learn personal team dev in that repo by sharing files and using issues.

8. Start to learn personal team dev in that repo by sharing files and using issues.

9. After reviewing existing team-dev-dojo project issues, create new project [issue](https://guides.github.com/features/issues/).

10.  Create [pull request](https://yangsu.github.io/pull-request-tutorial/).

11. Create conflicts and see how to undo, prevent.

12. Start from the top of this page and ensure that all is clear.
* The one thing that we did not cover was starting locally

13. Add the bitbucket project repo as the origin remote:
```
$ git remote rename origin public-downstream
$ git remote add origin https://bitbucket.org/urbanspectra/team-dev-dojo/
$ git remote -v
```
14. Now when we push or pull, we must indicate to which remote:
```
$ git remote pull origin
```

15. Review all of this and start again to ensure that you have enough practice to help someone else.

16. We still need to learn about branch, merge and rebase.
* [Gitflow reading and diagrams](http://nvie.com/posts/a-successful-git-branching-model/) will assist with that. 

17. Add and support someone though this process to get credit for further team dojo help.

---

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### What do we fix next in this repo?  ###
* [x]: Add the github public version as downstream remote.
* [ ]: Add two workflow diagrams:
    - [x]: Solo Local Git Workflow
    - [ ]: Solo Local Git Test-Driven Workflow
* [ ]: Push to github public and mess up and restore public team-dev-dojo repo from origin.
* [ ]: Clean up this page and add README.md formatting examples.
* [ ]: Review this page with a noob to ensure that all steps are clear.
* [ ]: Here we learn to screw it up, clean it up, and then prevent it going forwards as a team.
